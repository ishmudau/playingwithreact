import React, { Component } from "react";
import { LinkContainer } from "react-router-bootstrap";
import { PageHeader, ListGroup, ListGroupItem } from "react-bootstrap";
import { API } from "aws-amplify";
import "./Home.css";

export default class Home extends Component {
  constructor(props) {
    super(props);

    this.state = {
      isLoading: true,
      users: []
    };
  }

  async componentDidMount() {
    if (!this.props.isAuthenticated) {
      return;
    }
  
    try {
      const users = await this.users();
      this.setState({ users });
    } catch (e) {
      alert(e);
    }
  
    this.setState({ isLoading: false });
  }
  
  users() {
    return API.get("users", "/users");
  }
  

  renderUsersList(users) {
    return [{}].concat(users).map(
      (user, i) =>
        i !== 0
          ? <LinkContainer
              key={user.userId}
              to={`/users/${user.userId}`}
            >
              <ListGroupItem header={user.content.trim().split("\n")[0]}>
                {"Created: " + new Date(user.createdAt).toLocaleString()}
              </ListGroupItem>
            </LinkContainer>
          : <LinkContainer
              key="new"
              to="/users/new"
            >
              <ListGroupItem>
                <h4>
                  <b>{"\uFF0B"}</b> Create a new user
                </h4>
              </ListGroupItem>
            </LinkContainer>
    );
  }
  
  renderLander() {
    return (
      <div className="lander">
        <h1>Scratch</h1>
        <p>A simple user taking app</p>
      </div>
    );
  }

  renderUsers() {
    return (
      <div className="users">
        <PageHeader>Your Users</PageHeader>
        <ListGroup>
          {!this.state.isLoading && this.renderUsersList(this.state.users)}
        </ListGroup>
      </div>
    );
  }

  render() {
    return (
      <div className="Home">
        {this.props.isAuthenticated ? this.renderUsers() : this.renderLander()}
      </div>
    );
  }
}
